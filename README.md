# Ajera API PHP Library

Contact: me@mattlibera.com

# Introduction

This package is a PHP library used to interact with the Ajera API, documented here: [http://learningcenter.axium.com/public/APIDocumentation/index.html](http://learningcenter.axium.com/public/APIDocumentation/index.html).

## Scope

This package is built not (yet) as a comprehensive interface with the Ajera API - it is not written to perform every conceivable API call to Ajera. Specifically, this package was built to suit the needs of a particular league that I am commissioning, which uses advanced features that Ajera cannot natively support.

Please fork and add methods to this as you see fit / as your needs dictate.

# Installation

1. `composer require 'mattlibera/ajera-api-php-library'`
2. Use one of the API calls built into the `MattLibera\AjeraApi\AjeraApi` class, or extend it and add your own. The base API call is built in... you'll have to find a way in whatever framework / app you're working in to call the setter methods to set the variables required to make the call - specific to your environment (e.g. urls, credentials, etc.)

# Version History

### 0.1

Getting started. Available functionality:

- API call abstraction and laying groundwork
