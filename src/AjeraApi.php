<?php

namespace MattLibera\AjeraApi;

use App\Exceptions\ApiUpdateException;
use GuzzleHttp\Client;

class AjeraApi
{

    /*
    |--------------------------------------------------------------------------
    | Properties
    |--------------------------------------------------------------------------
    |
    | For abstraction these properties will hold the relevant environment and
    | auth info. Set these in a wrapper or whatever other framework you're
    | using.
    |
    */

    const MAX_RECORD_GET_SIZE = 100; // Ajera API can only accept 100 records in a get() operation

    // These come straight from the documentation: http://learningcenter.axium.com/public/APIDocumentation/index.html
    protected $id; // /AjeraAPI.ashx?{id} - leave off the trailing =
    protected $protocol; // http or https
    protected $host; // {host}/AjeraAPI.ashx - no trailing slash (usually my.site.com/ajera)

    protected $username;
    protected $password;

    protected $proxyHost;
    protected $proxyPort;

    protected $useProxy = false;

    protected $sessionToken = ''; // from Ajera API or handed in from app

    protected $returnRaw = false; // return raw call vs. extracted data

    /*
    |--------------------------------------------------------------------------
    | Helpers
    |--------------------------------------------------------------------------
    */


    /**
     * checkForSessionToken()
     *
     * gets session token if it is not already set. calls startSession() which by default will fetch new
     * from the Ajera API. user app can override this method in a child class to define the way in which
     * the app fetches the session (e.g. if the app stores the token in the browser session, or the
     * database, the app code can extend this class and redefine checkForSessionToken() to do what it
     * needs).
     *
     * @throws \Exception
     */
    protected function checkForSessionToken()
    {
        if (empty($this->sessionToken)) {
            $this->startSession();
        }
    }

    /**
     * castIds()
     *
     * takes an array of IDs and ensures they are integers (strings will cause errors from the API).
     * should be used for Get* endpoints.
     *
     * @param $array
     *
     * @return array
     */
    protected function castIds($array)
    {
        if (!is_array($array)) {
            $array = [$array];
        }

        foreach ($array as &$id) {
            $id = (int)$id;
        }

        return $array;
    }

    /**
     * prepareIds()
     *
     * takes an array of IDs and chunks it up into 100
     *
     * @param $array
     *
     * @return array
     */
    protected function prepareIds($array)
    {
        return array_map([$this, 'castIds'], array_chunk($array, self::MAX_RECORD_GET_SIZE));
    }


    /*
    |--------------------------------------------------------------------------
    | Setters
    |--------------------------------------------------------------------------
    */

    /**
     * @param $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @param $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @param $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param $protocol
     */
    public function setProtocol($protocol)
    {
        $this->protocol = $protocol;
    }

    /**
     * @param mixed $host
     */
    public function setHost($host)
    {
        $this->host = $host;
    }

    /**
     * @param mixed $proxyHost
     */
    public function setProxyHost($proxyHost)
    {
        $this->proxyHost = $proxyHost;
    }

    /**
     * @param mixed $proxyPort
     */
    public function setProxyPort($proxyPort)
    {
        $this->proxyPort = $proxyPort;
    }

    /**
     * @param boolean $useProxy
     */
    public function setUseProxy($useProxy)
    {
        $this->useProxy = ($useProxy === true);
    }

    /**
     * @param boolean $returnRaw
     */
    public function setReturnRaw($returnRaw)
    {
        $this->returnRaw = ($returnRaw === true);
    }

    /**
     * @param boolean $returnRaw
     */
    public function setSessionToken($sessionToken)
    {
        $this->sessionToken = $sessionToken;
    }

    /*
    |--------------------------------------------------------------------------
    | API Calls
    |--------------------------------------------------------------------------
    |
    | Methods that actually perform the API calls to Ajera APIs. Not callable
    | directly; only internally by other class methods.
    |
    */

    /**
     * apiCall() - performs a call to the Ajera API
     *
     * Notes: all API calls are the POST method. All data passed in is in JSON format.
     *
     * @param string $method - command type (CreateAPISession, ListClients, etc.)
     * @param array $methodArguments - array that will be sent as JSON to the API endpoint.
     * @param array $requestOptions - Guzzle options for the request.
     *
     * @return array
     */

    protected function apiCall($method, $methodArguments = [], $requestOptions = [])
    {
        try {
            $requiredProperties = ['id', 'host'];
            foreach ($requiredProperties as $property) {
                if ($this->$property === null) {
                    throw new \Exception("Error: required property '$property' has not been set in API object.");
                }
            }

            // assemble the request target and endpoint
            $endpoint = $this->protocol . '://' . $this->host . '/AjeraAPI.ashx?' . $this->id . '=';

            // instantiate Guzzle client with headers
            $client = new Client([
                [
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Accept'       => 'application/json'
                    ]
                ]
            ]);

            $requiredBodyParams = ['Method' => $method];

            // get session token unless it's a request for a new one.
            if ($method != 'CreateAPISession') {
                $this->checkForSessionToken();
                $requiredBodyParams['SessionToken'] = $this->sessionToken;
            }

            // transmute empty argument arrays to objects
            if (isset($methodArguments['MethodArguments']) && empty($methodArguments['MethodArguments'])) {
                $methodArguments['MethodArguments'] = new \stdClass();
            }

            // request body
            $finalBodyArray = array_merge($requiredBodyParams, $methodArguments);
            $requestOptions['json'] = $finalBodyArray;

            // if ($method == "UpdateProjects") {
            //     echo "<pre>";
            //     dd(print_r(json_encode($finalBodyArray)));
            // }

            // use proxy if it is set
            if ($this->useProxy) {
                $requestOptions['proxy'] = $this->proxyHost . ':' . $this->proxyPort;
            }

            // perform the call
            $response = $client->post($endpoint, $requestOptions);

            // get response data
            $code = $response->getStatusCode();
            $reason = $response->getReasonPhrase();
            $body = $response->getBody();
            $bodyContents = json_decode($body->getContents(), true);

            // parse basic info for all requests and return request data
            return [
                'response' => [
                    'endpoint'     => $endpoint,
                    'method'       => $method,
                    'responseCode' => $bodyContents['ResponseCode'],
                    'success'      => $bodyContents['ResponseCode'] === 200,
                    'message'      => $bodyContents['Content']['message'] ?? $bodyContents['Message'],
                    'errors'       => json_encode($bodyContents['Errors']),
                    'httpCode'     => $code,
                    'httpReason'   => $reason
                ],
                'body'     => $bodyContents
            ];
        } catch (\Exception $e) {
            return [
                'error' => $e->getMessage()
            ];
        }
    }

    /*
    |--------------------------------------------------------------------------
    | Authorization / Login
    |--------------------------------------------------------------------------
    |
    | Log a user in (get session token)
    |
    */

    /**
     * startSession() - gets a new session token from the Ajera API
     *
     * @param string $username
     * @param string $password
     * @param bool $raw
     *
     * @return array|bool
     * @throws \Exception
     */
    public function startSession()
    {
        $body = [
            'Username'         => $this->username,
            'Password'         => $this->password,
            'APIVersion'       => 1,
            'UseSessionCookie' => false
        ];

        $result = $this->apiCall('CreateAPISession', $body);
        if ($result['response']['success']) {
            // store the token in this instance of the object so that we can make other calls
            $this->sessionToken = $result['body']['Content']['SessionToken'];
        } else {
            throw new \Exception('Ajera API error: ' . $result['response']['errors']);
        }

        // hand back token so that the app can decide how to store it persistently
        return $this->returnRaw ? $result : $this->sessionToken;
    }

    /**
     * endSession() - ends a session from the Ajera API
     *
     * @param string $sessionToken
     * @param bool $raw
     *
     * @return array|bool
     * @throws \Exception
     */
    public function endSession()
    {
        $body = [

        ];

        $result = $this->apiCall('EndAPISession', $body);
        if (!$result['response']['success']) {
            throw new \Exception('Ajera API error: ' . $result['response']['errors']);
        }

        return $this->returnRaw ? $result : true;
    }

    /*
    |--------------------------------------------------------------------------
    | Clients
    |--------------------------------------------------------------------------
    |
    | Operations on Clients
    |
    */

    public function listClients($methodArguments = [])
    {
        $body = [
            'MethodArguments' => $methodArguments
        ];

        $result = $this->apiCall('ListClients', $body);

        if ($result['response']['success']) {
            return $this->returnRaw ? $result : $result['body']['Content']['Clients'];
        } else {
            return [];
        }
    }

    public function getClients($requestedClientIds = [])
    {
        if ($requestedClientIds == 'all') {
            $clients = $this->listClients();
            $ids = [];
            foreach ($clients as $client) {
                $ids[] = $client['ClientKey'];
            }
            $requestedClientIds = $ids;
        }

        // get an array chunked into 100
        $idGroups = $this->prepareIds($requestedClientIds);

        $clients = [];

        foreach ($idGroups as $ids) {
            $body = [
                'MethodArguments' => [
                    "RequestedClients" => $ids
                ]
            ];

            $result = $this->apiCall('GetClients', $body);

            if ($result['response']['success']) {
                // return $this->returnRaw ? $result : $result['body']['Content']['Clients'];
                $clients = array_merge($clients, $result['body']['Content']['Clients']);
            } else {
                throw new ApiUpdateException('Error during getClients call');
            }
        }

        return $clients;
    }

    public function getAllClients()
    {
        return $this->getClients('all');
    }

    public function createClient($details, $useSingleTransaction = false)
    {
        $requiredFields = ['Description', 'Status'];

        foreach ($requiredFields as $field) {
            if (!array_key_exists($field, $details)) {
                throw new \Exception("Required field '$field' is not present in creation array");
            }
        }

        $updatedClientsBaseArray = [
            'ClientKey'        => -1,
            'LastModifiedDate' => date('Y-m-d H:i:s \G\M\TO'),
            'Delete'           => false
        ];

        $updatedClientsArray = array_merge($updatedClientsBaseArray, $details);

        $body = [
            'MethodArguments' => [
                'UpdatedClients'       => [
                    $updatedClientsArray
                ],
                'UnchangedClients'     => [],
                'UseSingleTransaction' => $useSingleTransaction
            ]
        ];

        $result = $this->apiCall('UpdateClients', $body);

        if ($result['response']['success']) {
            return $this->returnRaw ? $result : $result['body']['Content']['Clients'];
        } else {
            return false;
        }
    }

    public function updateClient($id, $updateArray = [], $useSingleTransaction = false)
    {
        // to update, we need to first fetch the current data on the record and pass it into the unchanged clients array, while also passing in our operational data into the updatedclients array.

        // now need to get all client details
        $clientDetails = $this->getClients([$id]);
//        dd($clientDetails);

        $updatedClientsArray = [
            'ClientKey'        => $id,
            'Description'      => $updateArray['Description'] ?? $clientDetails[0]['Description'],
            'LastModifiedDate' => $updateArray['LastModifiedDate'] ?? date('Y-m-d H:i:s \G\M\TO'),
            'Delete'           => false,
            'Status'           => $updateArray['Status'] ?? $clientDetails[0]['Status']
        ];

//        dd($updatedClientsArray);

        $body = [
            'MethodArguments' => [
                'UpdatedClients'       => [
                    $updatedClientsArray
                ],
                'UnchangedClients'     => $clientDetails,
                'UseSingleTransaction' => $useSingleTransaction
            ]
        ];

        $result = $this->apiCall('UpdateClients', $body);

        if ($result['response']['success']) {
            return $this->returnRaw ? $result : $result['body']['Content']['Clients'];
        } else {
            return false;
        }
    }

    public function deleteClient($id, $useSingleTransaction = false)
    {
        // to delete, we need to first fetch the current data on the record and pass it into the unchanged clients array, while also passing in our operational data into the updatedclients array.

        // now need to get all client details
        $clientDetails = $this->getClients([$id]);
        // dd($clientDetails);

        $body = [
            'MethodArguments' => [
                'UpdatedClients'       => [
                    [
                        'ClientKey'        => $id,
                        'Description'      => $clientDetails[0]['Description'],
                        'LastModifiedDate' => $clientDetails[0]['LastModifiedDate'],
                        'Delete'           => true,
                        'Status'           => $clientDetails[0]['Status']
                    ]
                ],
                'UnchangedClients'     => $clientDetails,
                'UseSingleTransaction' => $useSingleTransaction
            ]
        ];

        $result = $this->apiCall('UpdateClients', $body);

        if ($result['response']['success']) {
            return $this->returnRaw ? $result : $result['body']['Content']['Clients'];
        } else {
            return $result;
            // return false;
        }
    }

    public function listClientTypes($status = ['active'])
    {
        $body = [
            'MethodArguments' => [
                'FilterByStatus' => $status
            ]
        ];

        $result = $this->apiCall('ListClientTypes', $body);

        if ($result['response']['success']) {
            return $this->returnRaw ? $result : $result['body']['Content']['ClientTypes'];
        } else {
            return [];
        }
    }

    /*
    |--------------------------------------------------------------------------
    | Contacts
    |--------------------------------------------------------------------------
    |
    | Operations on Contacts
    |
    */

    public function listContacts($methodArguments = [])
    {
        $body = [
            'MethodArguments' => $methodArguments
        ];

        $result = $this->apiCall('ListContacts', $body);

        if ($result['response']['success']) {
            return $this->returnRaw ? $result : $result['body']['Content']['Contacts'];
        } else {
            return [];
        }
    }

    // THIS IS BACKWARD. DON'T BE AN IDIOT. SAVING FOR WHEN I MIGHT ACTUALLY NEED THIS LOGIC

    // public function listContactsForClientId($clientId)
    // {
    //     $client = $this->getClients($clientId);

    //     if ($result['response']['success']) {
    //         $contacts = $client['Contacts'];
    //         // $contacts = $result['body']['Content']['Contacts'];

    //         // // need to get Clients as well so that we can filter.
    //         // $clients = $this->getAllClients();

    //         // $filtered = [];
    //         // foreach ($clients as $client) {
    //         //     foreach ($client['Contacts'] as $contact) {
    //         //         if ($contact['ContactKey'] == $clientId) {
    //         //             $filtered[] = $contact;
    //         //             continue;
    //         //         }
    //         //     }
    //         // }

    //         return $this->returnRaw ? $result : $contacts;
    //     } else {
    //         return [];
    //     }
    // }

    public function getContacts($requestedContactIds = [])
    {
        if ($requestedContactIds == 'all') {
            $contacts = $this->listContacts();
            $ids = [];
            foreach ($contacts as $contact) {
                $ids[] = $contact['ContactKey'];
            }
            $requestedContactIds = $ids;
        }

        // get an array chunked into 100
        $idGroups = $this->prepareIds($requestedContactIds);

        $contacts = [];

        foreach ($idGroups as $ids) {
            $body = [
                'MethodArguments' => [
                    "RequestedContacts" => $ids
                ]
            ];

            $result = $this->apiCall('GetContacts', $body);

            if ($result['response']['success']) {
                // return $this->returnRaw ? $result : $result['body']['Content']['Contacts'];
                $contacts = array_merge($contacts, $result['body']['Content']['Contacts']);
            } else {
                throw new ApiUpdateException('Error during getContacts call');
            }
        }

        return $contacts;
    }

    public function getAllContacts()
    {
        return $this->getContacts('all');
    }

    public function createContact($details, $useSingleTransaction = false)
    {
        $requiredFields = isset($details['Company']) ? ['Company'] : ['FirstName', 'LastName'];

        foreach ($requiredFields as $field) {
            if (!array_key_exists($field, $details)) {
                throw new \Exception("Required field '$field' is not present in creation array");
            }
        }

        $updatedContactsBaseArray = [
            'ContactKey'        => -1,
            'LastModifiedDate'  => date('Y-m-d H:i:s \G\M\TO'),
            'Delete'            => false
        ];

        $updatedContactsArray = array_merge($updatedContactsBaseArray, $details);

        $body = [
            'MethodArguments' => [
                'UpdatedContacts'       => [
                    $updatedContactsArray
                ],
                'UnchangedContacts'     => [],
                'UseSingleTransaction'  => $useSingleTransaction
            ]
        ];

        $result = $this->apiCall('UpdateContacts', $body);

        if ($result['response']['success']) {
            return $this->returnRaw ? $result : $result['body']['Content']['Contacts'];
        } else {
            return false;
        }
    }

    public function deleteContact($id, $useSingleTransaction = false)
    {
        // before deleting, we're going to need to figure out if this record is attached to anything else. This is going to be a pain.
        // Contacts can be attached to: employees, phases, projects, clients

        // to delete, we need to first fetch the current data on the record and pass it into the unchanged projects array, while also passing in our operational data into the updatedprojects array.

        // now need to get all project details
        $projectDetails = $this->getContacts([$id]);
//        dd($projectDetails);

        $body = [
            'MethodArguments' => [
                'UpdatedContacts'      => [
                    [
                        'ContactKey'       => $id,
                        'LastModifiedDate' => $projectDetails[0]['LastModifiedDate'],
                        'Delete'           => true,
                    ]
                ],
                'UnchangedContacts'    => $projectDetails,
                'UseSingleTransaction' => $useSingleTransaction
            ]
        ];

        $result = $this->apiCall('UpdateContacts', $body);

        if ($result['response']['success']) {
            return $this->returnRaw ? $result : $result['body']['Content']['Contacts'];
        } else {
            return $result;
            // return false;
        }
    }

    /*
    |--------------------------------------------------------------------------
    | Employees
    |--------------------------------------------------------------------------
    |
    | Operations on Employees
    |
    */

    public function listEmployees($methodArguments = [])
    {
        $body = [
            'MethodArguments' => $methodArguments
        ];

        $result = $this->apiCall('ListEmployees', $body);

        if ($result['response']['success']) {
            return $this->returnRaw ? $result : $result['body']['Content']['Employees'];
        } else {
            return [];
        }
    }

    public function getEmployees($requestedEmployeeIds = [])
    {
        if ($requestedEmployeeIds == 'all') {
            $contacts = $this->listEmployees();
            $ids = [];
            foreach ($contacts as $contact) {
                $ids[] = $contact['EmployeeKey'];
            }
            $requestedEmployeeIds = $ids;
        }

        // get an array chunked into 100
        $idGroups = $this->prepareIds($requestedEmployeeIds);

        $employees = [];

        foreach ($idGroups as $ids) {
            $body = [
                'MethodArguments' => [
                    "RequestedEmployees" => $ids
                ]
            ];

            $result = $this->apiCall('GetEmployees', $body);

            if ($result['response']['success']) {
                // return $this->returnRaw ? $result : $result['body']['Content']['Employees'];
                $employees = array_merge($employees, $result['body']['Content']['Employees']);
            } else {
                throw new ApiUpdateException('Error during getEmployees call');
            }
        }

        return $employees;
    }

    public function getAllEmployees()
    {
        return $this->getEmployees('all');
    }

    // "delete is not supported for employees." - um, ok.
//     public function deleteEmployee($id, $useSingleTransaction = false)
//     {
//         // to delete, we need to first fetch the current data on the record and pass it into the unchanged projects array, while also passing in our operational data into the updatedprojects array.
//
//         // now need to get all project details
//         $projectDetails = $this->getEmployees([$id]);
    // //        dd($projectDetails);
//
//         $body = [
//             'MethodArguments' => [
//                 'UpdatedEmployees'     => [
//                     [
//                         'EmployeeKey'      => $id,
//                         'LastModifiedDate' => $projectDetails[0]['LastModifiedDate'],
//                         'Delete'           => true,
//                     ]
//                 ],
//                 'UnchangedEmployees'   => $projectDetails,
//                 'UseSingleTransaction' => $useSingleTransaction
//             ]
//         ];
//
//         $result = $this->apiCall('UpdateEmployees', $body);
//
//         if ($result['response']['success']) {
//             return $this->returnRaw ? $result : $result['body']['Content']['Employees'];
//         } else {
//             return $result;
//             // return false;
//         }
//     }


    /*
    |--------------------------------------------------------------------------
    | Vendors
    |--------------------------------------------------------------------------
    |
    | Operations on Vendors
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Projects
    |--------------------------------------------------------------------------
    |
    | Operations on Projects
    |
    */

    public function listProjects($methodArguments = [])
    {
        $body = [
            'MethodArguments' => $methodArguments
        ];

        $result = $this->apiCall('ListProjects', $body);

        if ($result['response']['success']) {
            return $this->returnRaw ? $result : $result['body']['Content']['Projects'];
        } else {
            return [];
        }
    }

    public function getProjects($requestedProjectIds = [])
    {
        if ($requestedProjectIds == 'all') {
            $projects = $this->listProjects();
            $ids = [];
            foreach ($projects as $project) {
                $ids[] = $project['ProjectKey'];
            }
            $requestedProjectIds = $ids;
        }

        // get an array chunked into 100
        $idGroups = $this->prepareIds($requestedProjectIds);

        $projects = [];

        foreach ($idGroups as $ids) {
            $body = [
                'MethodArguments' => [
                    "RequestedProjects" => $ids
                ]
            ];

            $result = $this->apiCall('GetProjects', $body);

            if ($result['response']['success']) {
                // return $this->returnRaw ? $result : $result['body']['Content']['Projects'];
                $projects = array_merge($projects, $result['body']['Content']['Projects']);
            } else {
                throw new ApiUpdateException('Error during getProjects call');
            }
        }

        return $projects;
    }

    public function getAllProjects()
    {
        return $this->getProjects('all');
    }

    public function createProject($details, $useSingleTransaction = false)
    {
        $requiredFields = ['Description', 'BillingType', 'CompanyKey', 'Contacts', 'InvoiceGroups'];

        foreach ($requiredFields as $field) {
            if (!array_key_exists($field, $details)) {
                throw new \Exception("Required field '$field' is not present in creation array");
            }
        }

        $updatedProjectsBaseArray = [
            'ProjectKey'       => -1,
            'LastModifiedDate' => date('Y-m-d H:i:s \G\M\TO')
        ];

        $updatedProjectsArray = array_merge($updatedProjectsBaseArray, $details);

        $body = [
            'MethodArguments' => [
                'UpdatedProjects'      => [
                    $updatedProjectsArray
                ],
                'UnchangedProjects'    => [],
                'UseSingleTransaction' => $useSingleTransaction
            ]
        ];

        $result = $this->apiCall('UpdateProjects', $body);

        if ($result['response']['success']) {
            return $this->returnRaw ? $result : $result['body']['Content']['Projects'];
        } else {
            return $result;
        }
    }

    public function updateProject($id, $updateArray = [], $useSingleTransaction = false)
    {
        // to update, we need to first fetch the current data on the record and pass it into the unchanged clients array, while also passing in our operational data into the updatedclients array.

        // now need to get all project details
        $projectDetails = $this->getProjects([$id]);

        $updatedProjectsArray = [
            'ProjectKey'       => $id,
            'Description'      => $updateArray['Description'] ?? $projectDetails[0]['Description'],
            'LastModifiedDate' => $updateArray['LastModifiedDate'] ?? date('Y-m-d H:i:s \G\M\TO'),
            'Delete'           => false,
            'CompanyKey'       => $updateArray['CompanyKey'] ?? $projectDetails[0]['CompanyKey'],
            'Contacts'         => $updateArray['Contacts'] ?? [],
            'InvoiceGroups'    => $updateArray['InvoiceGroups'] ?? [],
        ];

        $body = [
            'MethodArguments' => [
                'UpdatedProjects'      => [
                    $updatedProjectsArray
                ],
                'UnchangedProjects'    => $projectDetails,
                'UseSingleTransaction' => $useSingleTransaction
            ]
        ];

        $result = $this->apiCall('UpdateProjects', $body);

        if ($result['response']['success']) {
            return $this->returnRaw ? $result : $result['body']['Content']['Projects'];
        } else {
            return $result;
        }
    }

    public function deleteProject($id, $useSingleTransaction = false)
    {
        // to delete, we need to first fetch the current data on the record and pass it into the unchanged projects array, while also passing in our operational data into the updatedprojects array.

        // now need to get all project details
        $projectDetails = $this->getProjects([$id]);
//        dd($projectDetails);

        $body = [
            'MethodArguments' => [
                'UpdatedProjects'      => [
                    [
                        'ProjectKey'       => $id,
                        'LastModifiedDate' => $projectDetails[0]['LastModifiedDate'],
                        'Delete'           => true,
                    ]
                ],
                'UnchangedProjects'    => $projectDetails,
                'UseSingleTransaction' => $useSingleTransaction
            ]
        ];

        $result = $this->apiCall('UpdateProjects', $body);

        if ($result['response']['success']) {
            return $this->returnRaw ? $result : $result['body']['Content']['Projects'];
        } else {
            return false;
        }
    }

    public function getProjectTotals($projectId)
    {
        $body = [
            'MethodArguments' => [
                "RequestedProjectTotals" => $projectId
            ]
        ];

        $result = $this->apiCall('GetProjectTotals', $body);

        if ($result['response']['success']) {
            return $this->returnRaw ? $result : $result['body']['Content']['ProjectTotals'];
        } else {
            return false;
        }
    }

    /*
    |--------------------------------------------------------------------------
    | Expense Reports
    |--------------------------------------------------------------------------
    |
    | Operations on Expense Reports
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Various
    |--------------------------------------------------------------------------
    |
    | Operations on Various List Methods
    |
    */

    public function listCompanies($methodArguments = [])
    {
        $body = [
            'MethodArguments' => $methodArguments
        ];

        $result = $this->apiCall('ListCompanies', $body);

        if ($result['response']['success']) {
            return $this->returnRaw ? $result : $result['body']['Content']['Companies'];
        } else {
            return [];
        }
    }

    public function listInvoiceFormats($methodArguments = [])
    {
        $body = [
            'MethodArguments' => $methodArguments
        ];

        $result = $this->apiCall('ListInvoiceFormats', $body);

        if ($result['response']['success']) {
            return $this->returnRaw ? $result : $result['body']['Content']['InvoiceFormats'];
        } else {
            return [];
        }
    }
}
